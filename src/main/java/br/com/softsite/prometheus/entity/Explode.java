package br.com.softsite.prometheus.entity;

import br.com.softsite.toolbox.IsSerializable;

public class Explode implements IsSerializable {

	private String message;
	private String explodeType;

	Explode() {
	}

	public Explode(String message) {
		this.message = message;
	}

	public Explode(String message, String explodeType) {
		this.message = message;
		this.explodeType = explodeType;
	}

	public String getMessage() {
		return this.message;
	}

	public String getExplodeType() {
		return explodeType;
	}

	@Override
	public String toString() {
		return getMessage();
	}
}