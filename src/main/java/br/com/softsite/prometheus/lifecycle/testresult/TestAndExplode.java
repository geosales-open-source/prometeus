package br.com.softsite.prometheus.lifecycle.testresult;

import static br.com.softsite.prometheus.lifecycle.testresult.TestAndResult.singleToBi;

import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

import br.com.softsite.prometheus.entity.Explode;
import br.com.softsite.prometheus.lifecycle.CycleCache;
import br.com.softsite.prometheus.lifecycle.Frankenstein;


public interface TestAndExplode<T> extends TestAndX<T, Explode> {

	Explode generateX(T t, CycleCache cc);

	default <U> TestAndExplode<U> transform(Function<U, T> transformer) {
		return basic((u, cc) -> this.test(transformer.apply(u), cc),
				(u, cc) -> this.generateX(transformer.apply(u), cc));
	}

	@Override
	default Frankenstein xToFrank(Explode explode) {
		return Frankenstein.fromExplode(explode);
	}

	static <T> TestAndExplode<T> basic(BiPredicate<T, CycleCache> imAlive, BiFunction<T, CycleCache, Explode> f) {
		return new TestAndExplode<T>() {

			@Override
			public Explode generateX(T t, CycleCache cycleCache) {
				return f.apply(t, cycleCache);
			}

			@Override
			public boolean test(T t, CycleCache cycleCache) {
				return imAlive.test(t, cycleCache);
			}
		};
	}

	static <T> TestAndExplode<T> basic(BiPredicate<T, CycleCache> imAlive, Function<T, Explode> f) {
		return basic(imAlive, singleToBi(f));
	}

	static <T> TestAndExplode<T> basic(Predicate<T> imAlive, BiFunction<T, CycleCache, Explode> f) {
		return basic(singleToBi(imAlive), f);
	}

	static <T> TestAndExplode<T> basic(Predicate<T> imAlive, Function<T, Explode> f) {
		return basic(singleToBi(imAlive), singleToBi(f));
	}
}
