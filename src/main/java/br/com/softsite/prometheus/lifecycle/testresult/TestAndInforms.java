package br.com.softsite.prometheus.lifecycle.testresult;

import static br.com.softsite.prometheus.lifecycle.testresult.TestAndResult.singleToBi;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

import br.com.softsite.prometheus.entity.Inform;
import br.com.softsite.prometheus.lifecycle.CycleCache;
import br.com.softsite.prometheus.lifecycle.Frankenstein;

public interface TestAndInforms<T> extends TestAndX<T, List<Inform>> {

	List<Inform> generateX(T t, CycleCache cc);

	default <U> TestAndInforms<U> transform(Function<U, T> transformer) {
		return basic((u, cc) -> this.test(transformer.apply(u), cc),
				(u, cc) -> this.generateX(transformer.apply(u), cc));
	}

	@Override
	default Frankenstein xToFrank(List<Inform> informs) {
		return Frankenstein.fromInforms(informs);
	}

	static <T> TestAndInforms<T> basic(BiPredicate<T, CycleCache> imAlive, BiFunction<T, CycleCache, List<Inform>> f) {
		return new TestAndInforms<T>() {

			@Override
			public List<Inform> generateX(T t, CycleCache cycleCache) {
				return f.apply(t, cycleCache);
			}

			@Override
			public boolean test(T t, CycleCache cycleCache) {
				return imAlive.test(t, cycleCache);
			}
		};
	}

	static <T> TestAndInforms<T> basic(BiPredicate<T, CycleCache> imAlive, Function<T, List<Inform>> f) {
		return basic(imAlive, singleToBi(f));
	}

	static <T> TestAndInforms<T> basic(Predicate<T> imAlive, BiFunction<T, CycleCache, List<Inform>> f) {
		return basic(singleToBi(imAlive), f);
	}

	static <T> TestAndInforms<T> basic(Predicate<T> imAlive, Function<T, List<Inform>> f) {
		return basic(singleToBi(imAlive), singleToBi(f));
	}
}
