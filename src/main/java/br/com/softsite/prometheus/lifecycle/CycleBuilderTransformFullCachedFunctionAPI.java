package br.com.softsite.prometheus.lifecycle;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

import br.com.softsite.prometheus.entity.Doubt;
import br.com.softsite.prometheus.entity.Explode;
import br.com.softsite.prometheus.entity.Inform;
import br.com.softsite.prometheus.lifecycle.testresult.*;

interface CycleBuilderTransformFullCachedFunctionAPI<T> extends CycleBuilderSelf<T> {

	default <C> CycleBuilder<T> addValidator(Function<T, C> transformer, Predicate<C> imalive, BiFunction<C, CycleCache, Frankenstein> validator) {
		return getSelf().addValidator(transformer, TestAndFrankenstein.basic(imalive, validator));
	}

	default <C> CycleBuilder<T> addValidatorFromExplode(Function<T, C> transformer, Predicate<C> imalive, BiFunction<C, CycleCache, Explode> validator) {
		return getSelf().addValidator(transformer, TestAndExplode.basic(imalive, validator));
	}

	default <C> CycleBuilder<T> addValidatorFromExplodes(Function<T, C> transformer, Predicate<C> imalive, BiFunction<C, CycleCache,  List<Explode>> validator) {
		return getSelf().addValidator(transformer, TestAndExplodes.basic(imalive, validator));
	}

	default <C> CycleBuilder<T> addValidatorFromDoubt(Function<T, C> transformer, Predicate<C> imalive, BiFunction<C, CycleCache,  Doubt> validator) {
		return getSelf().addValidator(transformer, TestAndDoubt.basic(imalive, validator));
	}

	default <C> CycleBuilder<T> addValidatorFromDoubts(Function<T, C> transformer, Predicate<C> imalive, BiFunction<C, CycleCache,  List<Doubt>> validator) {
		return getSelf().addValidator(transformer, TestAndDoubts.basic(imalive, validator));
	}

	default <C> CycleBuilder<T> addValidatorFromInform(Function<T, C> transformer, Predicate<C> imalive, BiFunction<C, CycleCache,  Inform> validator) {
		return getSelf().addValidator(transformer, TestAndInform.basic(imalive, validator));
	}

	default <C> CycleBuilder<T> addValidatorFromInforms(Function<T, C> transformer, Predicate<C> imalive, BiFunction<C, CycleCache,  List<Inform>> validator) {
		return getSelf().addValidator(transformer, TestAndInforms.basic(imalive, validator));
	}

	default <C> CycleBuilder<T> addModifier(Function<T, C> transformer, Predicate<C> imalive, BiConsumer<C, CycleCache> modifier) {
		return getSelf().addModifier(transformer, TestAndConsume.basic(imalive, modifier));
	}

	default <C> CycleBuilder<T> addModifierFromInform(Function<T, C> transformer, Predicate<C> imalive, BiFunction<C, CycleCache,  Inform> modifier) {
		return getSelf().addModifier(transformer, TestAndInform.basic(imalive, modifier));
	}

	default <C> CycleBuilder<T> addModifierFromInforms(Function<T, C> transformer, Predicate<C> imalive, BiFunction<C, CycleCache,  List<Inform>> modifier) {
		return getSelf().addModifier(transformer, TestAndInforms.basic(imalive, modifier));
	}

	default <C> CycleBuilder<T> addPersister(Function<T, C> transformer, Predicate<C> imalive, BiConsumer<C, CycleCache> persister) {
		return getSelf().addPersister(transformer, TestAndConsume.basic(imalive, persister));
	}

	default <C> CycleBuilder<T> addPersisterFromInform(Function<T, C> transformer, Predicate<C> imalive, BiFunction<C, CycleCache,  Inform> persister) {
		return getSelf().addPersister(transformer, TestAndInform.basic(imalive, persister));
	}

	default <C> CycleBuilder<T> addPersisterFromInforms(Function<T, C> transformer, Predicate<C> imalive, BiFunction<C, CycleCache,  List<Inform>> persister) {
		return getSelf().addPersister(transformer, TestAndInforms.basic(imalive, persister));
	}
}
