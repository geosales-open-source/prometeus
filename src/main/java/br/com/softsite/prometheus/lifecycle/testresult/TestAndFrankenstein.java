package br.com.softsite.prometheus.lifecycle.testresult;

import static br.com.softsite.prometheus.lifecycle.testresult.TestAndResult.singleToBi;

import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

import br.com.softsite.prometheus.lifecycle.CycleCache;
import br.com.softsite.prometheus.lifecycle.Frankenstein;

public interface TestAndFrankenstein<T> extends TestAndX<T, Frankenstein> {

	Frankenstein generateX(T t, CycleCache cc);

	default <U> TestAndFrankenstein<U> transform(Function<U, T> transformer) {
		return basic((u, cc) -> this.test(transformer.apply(u), cc),
				(u, cc) -> this.generateX(transformer.apply(u), cc));
	}

	@Override
	default Frankenstein xToFrank(Frankenstein f) {
		return f;
	}

	static <T> TestAndFrankenstein<T> basic(BiPredicate<T, CycleCache> imAlive, BiFunction<T, CycleCache, Frankenstein> f) {
		return new TestAndFrankenstein<T>() {

			@Override
			public Frankenstein generateX(T t, CycleCache cycleCache) {
				return f.apply(t, cycleCache);
			}

			@Override
			public boolean test(T t, CycleCache cycleCache) {
				return imAlive.test(t, cycleCache);
			}
		};
	}

	static <T> TestAndFrankenstein<T> basic(BiPredicate<T, CycleCache> imAlive, Function<T, Frankenstein> f) {
		return basic(imAlive, singleToBi(f));
	}

	static <T> TestAndFrankenstein<T> basic(Predicate<T> imAlive, BiFunction<T, CycleCache, Frankenstein> f) {
		return basic(singleToBi(imAlive), f);
	}

	static <T> TestAndFrankenstein<T> basic(Predicate<T> imAlive, Function<T, Frankenstein> f) {
		return basic(singleToBi(imAlive), singleToBi(f));
	}
}
