package br.com.softsite.prometheus.lifecycle;

import java.util.Collections;
import java.util.List;

import br.com.softsite.prometheus.entity.Doubt;
import br.com.softsite.prometheus.entity.Explode;
import br.com.softsite.prometheus.entity.Inform;
import br.com.softsite.prometheus.entity.PrometheusDTO;

public class Frankenstein {

	private static final Frankenstein EMPTY = new Frankenstein() {
		@Override

		public <T> PrometheusDTO<T> mergeInto(PrometheusDTO<T> prom) {
			return prom;
		}
	};

	public static Frankenstein getEmpty() {
		return EMPTY;
	}

	private final List<Explode> explodes;
	private final List<Doubt> doubts;
	private final List<Inform> informs;

	private Frankenstein() {
		explodes = Collections.emptyList();
		doubts = Collections.emptyList();
		informs = Collections.emptyList();
	}

	public static Frankenstein fromExplode(Explode explode) {
		if (explode == null) {
			return getEmpty();
		}
		return new Frankenstein(Collections.singletonList(explode), Collections.emptyList(), Collections.emptyList());
	}

	public static Frankenstein fromDoubt(Doubt doubt) {
		if (doubt == null) {
			return getEmpty();
		}
		return new Frankenstein(Collections.emptyList(), Collections.singletonList(doubt), Collections.emptyList());
	}

	public static Frankenstein fromInform(Inform inform) {
		if (inform == null) {
			return getEmpty();
		}
		return new Frankenstein(Collections.emptyList(), Collections.emptyList(), Collections.singletonList(inform));
	}

	public static Frankenstein fromExplodes(List<Explode> explodes) {
		if (explodes.isEmpty()) {
			return getEmpty();
		}
		return new Frankenstein(Collections.unmodifiableList(explodes), Collections.emptyList(), Collections.emptyList());
	}

	public static Frankenstein fromDoubts(List<Doubt> doubts) {
		if (doubts.isEmpty()) {
			return getEmpty();
		}
		return new Frankenstein(Collections.emptyList(), Collections.unmodifiableList(doubts), Collections.emptyList());
	}
	public static Frankenstein fromInforms(List<Inform> informs) {
		if (informs.isEmpty()) {
			return getEmpty();
		}
		return new Frankenstein(Collections.emptyList(), Collections.emptyList(), Collections.unmodifiableList(informs));
	}

	public static Frankenstein fromExplodesDoubtsInforms(List<Explode> explodes, List<Doubt> doubts, List<Inform> informs) {
		if (explodes.isEmpty() && doubts.isEmpty() && informs.isEmpty()) {
			return getEmpty();
		}
		return new Frankenstein(Collections.unmodifiableList(explodes), Collections.unmodifiableList(doubts), Collections.unmodifiableList(informs));
	}

	private Frankenstein(List<Explode> explodes, List<Doubt> doubts, List<Inform> informs) {
		this.explodes = explodes;
		this.doubts = doubts;
		this.informs = informs;
	}

	public <T> PrometheusDTO<T> mergeInto(PrometheusDTO<T> prom) {
		prom.addDoubts(this.doubts);
		prom.addExplodes(this.explodes);
		prom.addInforms(this.informs);

		return prom;
	}
}
