package br.com.softsite.prometheus.lifecycle.testresult;

import java.util.function.Function;

public interface TransformTestAndDoubt<T, C> {

	TestAndDoubt<C> getTestTransformBase();
	Function<T, C> getTransformation();

	default TestAndDoubt<T> transform() {
		return getTestTransformBase().transform(getTransformation());
	}

	static <T, C> TransformTestAndDoubt<T, C> basic(Function<T, C> transform, TestAndDoubt<C> test) {
		return new TransformTestAndDoubt<T, C>() {

			@Override
			public Function<T, C> getTransformation() {
				return transform;
			}

			@Override
			public TestAndDoubt<C> getTestTransformBase() {
				return test;
			}
		};
	}
}
