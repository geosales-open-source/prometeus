package br.com.softsite.prometheus.lifecycle;

public interface CycleCache {

	<T, K> T getCache(String cacheId, K key);
}
