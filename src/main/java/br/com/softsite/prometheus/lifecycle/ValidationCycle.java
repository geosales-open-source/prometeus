package br.com.softsite.prometheus.lifecycle;

import java.util.function.Consumer;
import java.util.function.Supplier;

import br.com.softsite.prometheus.entity.PrometheusDTO;
import br.com.softsite.toolbox.indirection.Indirection;

public class ValidationCycle<T> {

	private static interface TriFunction<A1, A2, A3, R> {
		R apply(A1 a1, A2 a2, A3 a3);
	}

	private final Pipeline<T> validators;
	private final Pipeline<T> modifiers;
	private final Pipeline<T> persisters;
	private final Supplier<CycleCache> cycleCacheSupplier;
	private final TriFunction<Pipeline<T>, PrometheusDTO<T>, CycleCache, PrometheusDTO<T>> runInTransactionAndReturn;

	public ValidationCycle(Pipeline<T> validators,
			Pipeline<T> modifiers,
			Pipeline<T> persisters) {
		this(validators, modifiers, persisters, Runnable::run);
	}

	public ValidationCycle(Pipeline<T> validators,
						   Pipeline<T> modifiers,
						   Pipeline<T> persisters,
						   Consumer<Runnable> runInTransaction) {
		this(validators, modifiers, persisters, runInTransaction, CycleCacheEmpty::getInstance);
	}

	public ValidationCycle(Pipeline<T> validators,
			Pipeline<T> modifiers,
			Pipeline<T> persisters,
			Consumer<Runnable> runInTransaction,
			Supplier<CycleCache> cycleCacheSupplier) {
		if (runInTransaction == null) {
			throw new NullPointerException("runInTransaction não pode ser nulo, o padrão é Runnable::run");
		}
		if (cycleCacheSupplier == null) {
			throw new NullPointerException("runInTransaction não pode ser nulo, o padrão é CycleCacheEmpty::getInstance");
		}
		this.validators = validators;
		this.modifiers = modifiers;
		this.persisters = persisters;
		this.cycleCacheSupplier = cycleCacheSupplier;

		this.runInTransactionAndReturn = (pipeline, dto, cache) -> {
			Indirection<PrometheusDTO<T>> ind = new Indirection<>(dto);
			runInTransaction.accept(() -> {
				ind.x = pipeline.run(dto, cache);
			});
			return ind.x;
		};
	}

	public PrometheusDTO<T> validate(T obj) {
		return validate(obj, getCycleCache());
	}

	private PrometheusDTO<T> validate(T obj, CycleCache cache) {
		return validators.run(obj, cache);
	}

	public PrometheusDTO<T> validateAndPersist(T obj) {
		CycleCache cache = getCycleCache();
		PrometheusDTO<T> dto = validate(obj, cache);

		if (dto.allOk()) {
			modifyAndPersist(dto, cache);
		}

		return dto;
	}

	public PrometheusDTO<T> modifyAndPersist(PrometheusDTO<T> dto) {
		return modifyAndPersist(dto, getCycleCache());
	}

	private PrometheusDTO<T> modifyAndPersist(PrometheusDTO<T> dto, CycleCache cache) {
		dto = modifiers.run(dto, cache);
		if (!persisters.isEmpty()) {
			dto = runInTransactionAndReturn.apply(persisters, dto, cache);
		}

		return dto;
	}

	private CycleCache getCycleCache() {
		return cycleCacheSupplier.get();
	}
}