package br.com.softsite.prometheus.lifecycle;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;

import br.com.softsite.prometheus.entity.Doubt;
import br.com.softsite.prometheus.entity.Explode;
import br.com.softsite.prometheus.entity.Inform;
import br.com.softsite.prometheus.lifecycle.testresult.*;

interface CycleBuilderCachedFullAPI<T> extends CycleBuilderSelf<T> {

	default CycleBuilder<T> addValidator(BiPredicate<T, CycleCache> imalive, BiFunction<T, CycleCache, Frankenstein> validator) {
		return getSelf().addValidator(TestAndFrankenstein.basic(imalive, validator));
	}

	default CycleBuilder<T> addValidatorFromExplode(BiPredicate<T, CycleCache> imalive, BiFunction<T, CycleCache, Explode> validator) {
		return getSelf().addValidator(TestAndExplode.basic(imalive, validator));
	}

	default CycleBuilder<T> addValidatorFromExplodes(BiPredicate<T, CycleCache> imalive, BiFunction<T, CycleCache, List<Explode>> validator) {
		return getSelf().addValidator(TestAndExplodes.basic(imalive, validator));
	}

	default CycleBuilder<T> addValidatorFromDoubt(BiPredicate<T, CycleCache> imalive, BiFunction<T, CycleCache, Doubt> validator) {
		return getSelf().addValidator(TestAndDoubt.basic(imalive, validator));
	}

	default CycleBuilder<T> addValidatorFromDoubts(BiPredicate<T, CycleCache> imalive, BiFunction<T, CycleCache, List<Doubt>> validator) {
		return getSelf().addValidator(TestAndDoubts.basic(imalive, validator));
	}

	default CycleBuilder<T> addValidatorFromInform(BiPredicate<T, CycleCache> imalive, BiFunction<T, CycleCache, Inform> validator) {
		return getSelf().addValidator(TestAndInform.basic(imalive, validator));
	}

	default CycleBuilder<T> addValidatorFromInforms(BiPredicate<T, CycleCache> imalive, BiFunction<T, CycleCache, List<Inform>> validator) {
		return getSelf().addValidator(TestAndInforms.basic(imalive, validator));
	}

	default CycleBuilder<T> addModifier(BiPredicate<T, CycleCache> imalive, BiConsumer<T, CycleCache> modifier) {
		return getSelf().addModifier(TestAndConsume.basic(imalive, modifier));
	}

	default CycleBuilder<T> addModifierFromInform(BiPredicate<T, CycleCache> imalive, BiFunction<T, CycleCache, Inform> modifier) {
		return getSelf().addModifier(TestAndInform.basic(imalive, modifier));
	}

	default  CycleBuilder<T> addModifierFromInforms(BiPredicate<T, CycleCache> imalive, BiFunction<T, CycleCache, List<Inform>> modifier) {
		return getSelf().addModifier(TestAndInforms.basic(imalive, modifier));
	}

	default CycleBuilder<T> addPersister(BiPredicate<T, CycleCache> imalive, BiConsumer<T, CycleCache> persister) {
		return getSelf().addModifier(TestAndConsume.basic(imalive, persister));
	}

	default CycleBuilder<T> addPersisterFromInform(BiPredicate<T, CycleCache> imalive, BiFunction<T, CycleCache, Inform> persister) {
		return getSelf().addModifier(TestAndInform.basic(imalive, persister));
	}

	default CycleBuilder<T> addPersisterFromInforms(BiPredicate<T, CycleCache> imalive, BiFunction<T, CycleCache, List<Inform>> persister) {
		return getSelf().addModifier(TestAndInforms.basic(imalive, persister));
	}
}
