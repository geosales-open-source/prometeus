package br.com.softsite.prometheus.lifecycle;

public class CycleCacheEmpty implements CycleCache {

	public static final CycleCacheEmpty INSTANCE = new CycleCacheEmpty();

	public static CycleCacheEmpty getInstance() {
		return INSTANCE;
	}

	private CycleCacheEmpty() {
	}

	@Override
	public <T, K> T getCache(String cacheId, K key) {
		return null;
	}
}
