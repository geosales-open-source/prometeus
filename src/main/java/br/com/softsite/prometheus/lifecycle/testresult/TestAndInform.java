package br.com.softsite.prometheus.lifecycle.testresult;

import static br.com.softsite.prometheus.lifecycle.testresult.TestAndResult.singleToBi;

import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

import br.com.softsite.prometheus.entity.Inform;
import br.com.softsite.prometheus.lifecycle.CycleCache;
import br.com.softsite.prometheus.lifecycle.Frankenstein;

public interface TestAndInform<T> extends TestAndX<T, Inform> {

	Inform generateX(T t, CycleCache cc);

	default <U> TestAndInform<U> transform(Function<U, T> transformer) {
		return basic((u, cc) -> this.test(transformer.apply(u), cc),
				(u, cc) -> this.generateX(transformer.apply(u), cc));
	}

	@Override
	default Frankenstein xToFrank(Inform inform) {
		return Frankenstein.fromInform(inform);
	}

	static <T> TestAndInform<T> basic(BiPredicate<T, CycleCache> imAlive, BiFunction<T, CycleCache, Inform> f) {
		return new TestAndInform<T>() {

			@Override
			public Inform generateX(T t, CycleCache cycleCache) {
				return f.apply(t, cycleCache);
			}

			@Override
			public boolean test(T t, CycleCache cycleCache) {
				return imAlive.test(t, cycleCache);
			}
		};
	}

	static <T> TestAndInform<T> basic(BiPredicate<T, CycleCache> imAlive, Function<T, Inform> f) {
		return basic(imAlive, singleToBi(f));
	}

	static <T> TestAndInform<T> basic(Predicate<T> imAlive, BiFunction<T, CycleCache, Inform> f) {
		return basic(singleToBi(imAlive), f);
	}

	static <T> TestAndInform<T> basic(Predicate<T> imAlive, Function<T, Inform> f) {
		return basic(singleToBi(imAlive), singleToBi(f));
	}
}
