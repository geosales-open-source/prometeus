package br.com.softsite.prometheus.lifecycle;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import br.com.softsite.prometheus.entity.Doubt;
import br.com.softsite.prometheus.entity.Explode;
import br.com.softsite.prometheus.entity.Inform;
import br.com.softsite.prometheus.lifecycle.testresult.*;

interface CycleBuilderBaseAPI<T> extends CycleBuilderSelf<T> {

	default CycleBuilder<T> addValidator(Predicate<T> imalive, Function<T, Frankenstein> validator) {
		return getSelf().addValidator(TestAndFrankenstein.basic(imalive, validator));
	}

	default CycleBuilder<T> addValidatorFromExplode(Predicate<T> imalive, Function<T, Explode> validator) {
		return getSelf().addValidator(TestAndExplode.basic(imalive, validator));
	}

	default CycleBuilder<T> addValidatorFromExplodes(Predicate<T> imalive, Function<T, List<Explode>> validator) {
		return getSelf().addValidator(TestAndExplodes.basic(imalive, validator));
	}

	default CycleBuilder<T> addValidatorFromDoubt(Predicate<T> imalive, Function<T, Doubt> validator) {
		return getSelf().addValidator(TestAndDoubt.basic(imalive, validator));
	}

	default CycleBuilder<T> addValidatorFromDoubts(Predicate<T> imalive, Function<T, List<Doubt>> validator) {
		return getSelf().addValidator(TestAndDoubts.basic(imalive, validator));
	}

	default CycleBuilder<T> addValidatorFromInform(Predicate<T> imalive, Function<T, Inform> validator) {
		return getSelf().addValidator(TestAndInform.basic(imalive, validator));
	}

	default CycleBuilder<T> addValidatorFromInforms(Predicate<T> imalive, Function<T, List<Inform>> validator) {
		return getSelf().addValidator(TestAndInforms.basic(imalive, validator));
	}

	default CycleBuilder<T> addModifier(Predicate<T> imalive, Consumer<T> modifier) {
		return getSelf().addModifier(TestAndConsume.basic(imalive, modifier));
	}

	default CycleBuilder<T> addModifierFromInform(Predicate<T> imalive, Function<T, Inform> modifier) {
		return getSelf().addModifier(TestAndInform.basic(imalive, modifier));
	}

	default CycleBuilder<T> addModifierFromInforms(Predicate<T> imalive, Function<T, List<Inform>> modifier) {
		return getSelf().addModifier(TestAndInforms.basic(imalive, modifier));
	}

	default CycleBuilder<T> addPersister(Predicate<T> imalive, Consumer<T> persister) {
		return getSelf().addPersister(TestAndConsume.basic(imalive, persister));
	}

	default CycleBuilder<T> addPersisterFromInform(Predicate<T> imalive, Function<T, Inform> persister) {
		return getSelf().addPersister(TestAndInform.basic(imalive, persister));
	}

	default CycleBuilder<T> addPersisterFromInforms(Predicate<T> imalive, Function<T, List<Inform>> persister) {
		return getSelf().addPersister(TestAndInforms.basic(imalive, persister));
	}
}
