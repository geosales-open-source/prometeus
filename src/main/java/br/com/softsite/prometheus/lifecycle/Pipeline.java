package br.com.softsite.prometheus.lifecycle;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

import br.com.softsite.prometheus.entity.PrometheusDTO;
import br.com.softsite.prometheus.lifecycle.testresult.TestAndConsume;
import br.com.softsite.prometheus.lifecycle.testresult.TestAndResult;

public abstract class Pipeline<T> {

	final Supplier<CycleCache> cacheSupplier;
	Pipeline(Supplier<CycleCache> cacheSupplier) {
		this.cacheSupplier = cacheSupplier;
	}

	public abstract boolean isEmpty();
	public PrometheusDTO<T> run(T arg) {
		return run(arg, null);
	}

	public PrometheusDTO<T> run(PrometheusDTO<T> arg) {
		return run(arg, null);
	}

	public abstract PrometheusDTO<T> run(T arg, CycleCache cache);
	public abstract PrometheusDTO<T> run(PrometheusDTO<T> arg, CycleCache cache);

	private static <T> void accumulate(PrometheusDTO<T> prometheus, Frankenstein frank) {
		frank.mergeInto(prometheus);
	}

	public static <T> Pipeline<T> build(List<TestAndResult<T>> stuff) {
		return build(stuff, CycleCacheEmpty::getInstance);
	}

	public static <T> Pipeline<T> build(List<TestAndResult<T>> stuff, Supplier<CycleCache> cacheSupplier) {
		int size = stuff.size();
		if (size == 0) {
			return PipelineEmptyTrivial.getTrivialEmpty();
		} else if (size == 1) {
			return new PipelineSingle<>(stuff.get(0), cacheSupplier);
		}
		return new PipelineCommon<>(stuff, cacheSupplier);
	}

	public static class PipelineBuilder<T> {

		private List<TestAndResult<T>> stuff = new ArrayList<>();
		private Supplier<CycleCache> supplierCache = CycleCacheEmpty::getInstance;

		public PipelineBuilder() {
		}

		public PipelineBuilder<T> addModifier(TestAndConsume<T> imaliveModifier) {
			stuff.add(imaliveModifier);
			return this;
		}

		public PipelineBuilder<T> addModifier(Predicate<T> imalive, Consumer<T> modifier) {
			return addModifier(TestAndConsume.basic(imalive, modifier));
		}

		public PipelineBuilder<T> addModifier(BiPredicate<T, CycleCache> imalive, Consumer<T> modifier) {
			return addModifier(TestAndConsume.basic(imalive, modifier));
		}

		public PipelineBuilder<T> addModifier(Predicate<T> imalive, BiConsumer<T, CycleCache> modifier) {
			return addModifier(TestAndConsume.basic(imalive, modifier));
		}

		public PipelineBuilder<T> addModifier(BiPredicate<T, CycleCache> imalive, BiConsumer<T, CycleCache> modifier) {
			return addModifier(TestAndConsume.basic(imalive, modifier));
		}

		public Pipeline<T> build() {
			return Pipeline.build(stuff, supplierCache);
		}
	}

	private static class PipelineCommon<T> extends Pipeline<T> {

		private final List<TestAndResult<T>> stuff;
		PipelineCommon(List<TestAndResult<T>> stuff, Supplier<CycleCache> cacheSupplier) {
			super(cacheSupplier);
			this.stuff = stuff;
		}

		@Override
		public PrometheusDTO<T> run(T arg, CycleCache cache) {
			return stuff.stream().filter(v -> v.test(arg, cache)).map(v -> v.apply(arg, cache)).collect(() -> new PrometheusDTO<>(arg),
						Pipeline::accumulate, PrometheusDTO::merge);
		}

		@Override
		public PrometheusDTO<T> run(PrometheusDTO<T> arg, CycleCache cache) {
			return arg.merge(run(arg.getObj(), cache));
		}

		@Override
		public boolean isEmpty() {
			return false;
		}
	}

	private static class PipelineSingle<T> extends Pipeline<T> {

		private final TestAndResult<T> testAndResult;
		PipelineSingle(TestAndResult<T> testAndResult, Supplier<CycleCache> cacheSupplier) {
			super(cacheSupplier);
			this.testAndResult = testAndResult;
		}

		@Override
		public PrometheusDTO<T> run(T arg, CycleCache cache) {
			PrometheusDTO<T> pdto = new PrometheusDTO<>(arg);
			if (testAndResult.test(arg, cache)) {
				testAndResult.apply(arg, cache).mergeInto(pdto);
			}
			return pdto;
		}

		@Override
		public PrometheusDTO<T> run(PrometheusDTO<T> pdto, CycleCache cache) {
			T obj = pdto.getObj();
			if (testAndResult.test(obj, cache)) {
				testAndResult.apply(obj, cache).mergeInto(pdto);
			}
			return pdto;
		}

		@Override
		public boolean isEmpty() {
			return false;
		}
	}

	private static class PipelineEmptyTrivial<T> extends Pipeline<T> {
		private static final PipelineEmptyTrivial<Object> TRIVIAL = new PipelineEmptyTrivial<>();

		@SuppressWarnings("unchecked")
		static <T> PipelineEmptyTrivial<T> getTrivialEmpty() {
			return (PipelineEmptyTrivial<T>) TRIVIAL;
		}

		private PipelineEmptyTrivial() {
			super(null);
		}

		@Override
		public PrometheusDTO<T> run(T arg, CycleCache cache) {
			return new PrometheusDTO<>(arg);
		}

		@Override
		public PrometheusDTO<T> run(PrometheusDTO<T> arg, CycleCache cache) {
			return arg;
		}

		@Override
		public boolean isEmpty() {
			return true;
		}
	}
}
