package br.com.softsite.prometheus;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import java.util.function.Function;

import org.junit.Before;
import org.junit.Test;

import br.com.softsite.prometheus.entity.Doubt;
import br.com.softsite.prometheus.entity.Explode;
import br.com.softsite.prometheus.entity.PrometheusDTO;
import br.com.softsite.prometheus.lifecycle.CycleBuilder;
import br.com.softsite.prometheus.lifecycle.ValidationCycle;

public class TransactionValidationTest {
	BooleanSupplier iniciouTransacao;
	BooleanSupplier deuCommit;
	BooleanSupplier deuRollback;
	BooleanSupplier persistiu1;
	BooleanSupplier persistiu2;
	BooleanSupplier naoDeveriaPersistir;

	Consumer<Runnable> pseudoTransaction;
	CycleBuilder<String> builder;

	@Before
	public void setup() {
		ArrayList<Runnable> coisasQuandoCommit = new ArrayList<>();
		naoDeveriaPersistir = persistiu1 = persistiu2 = deuCommit = deuRollback = iniciouTransacao = () -> false;
		pseudoTransaction = runner -> {
			try {
				iniciouTransacao = () -> true;
				runner.run();
				coisasQuandoCommit.forEach(Runnable::run);
				deuCommit = () -> true;
			} catch (Throwable t) {
				deuRollback = () -> true;
				throw t;
			}
		};
		builder = new CycleBuilder<>();
		builder.addPersister(x -> true, t -> coisasQuandoCommit.add(() -> persistiu1 = () -> true));
		builder.addPersister(x -> true, t -> coisasQuandoCommit.add(() -> persistiu2 = () -> true));
		builder.addPersister(x -> false, t -> naoDeveriaPersistir = () -> true);
		builder.setRunInTransaction(pseudoTransaction);
	}

	@Test(expected = NullPointerException.class)
	public void setNullRunInTransactionTest() {
		builder.setRunInTransaction(null);
	}

	@Test
	public void emptyTest() {
		assertFalse("no vazio não iniciou transação", iniciouTransacao.getAsBoolean());
		assertFalse("no vazio não commitou transação", deuCommit.getAsBoolean());
		assertFalse("no vazio não  rollback na transação", deuRollback.getAsBoolean());

		assertFalse("no vazio não persistir o primeiro", persistiu1.getAsBoolean());
		assertFalse("no vazio não persistir o segundo", persistiu2.getAsBoolean());
		assertFalse("esse persister jamais deveria ser chamado", naoDeveriaPersistir.getAsBoolean());
	}

	@Test
	public void salvaCommit() {
		builder.generateCycle().validateAndPersist("teste");

		assertTrue("iniciou transação", iniciouTransacao.getAsBoolean());
		assertTrue("commitou transação", deuCommit.getAsBoolean());
		assertFalse("sem rollback na transação", deuRollback.getAsBoolean());

		assertTrue("deveria persistir o primeiro", persistiu1.getAsBoolean());
		assertTrue("deveria persistir o segundo", persistiu2.getAsBoolean());
		assertFalse("esse persister jamais deveria ser chamado", naoDeveriaPersistir.getAsBoolean());
	}

	@Test
	public void explosaoRollback() {
		try {
			builder.addPersister(x -> true, t -> {
				throw new SkipTestException();
			}).generateCycle().validateAndPersist("teste");
		} catch (SkipTestException e) {
		}

		assertTrue("iniciou transação", iniciouTransacao.getAsBoolean());
		assertFalse("não commitou transação", deuCommit.getAsBoolean());
		assertTrue("rollback na transação", deuRollback.getAsBoolean());

		assertFalse("não deveria persistir o primeiro", persistiu1.getAsBoolean());
		assertFalse("não deveria persistir o segundo", persistiu2.getAsBoolean());
		assertFalse("esse persister jamais deveria ser chamado", naoDeveriaPersistir.getAsBoolean());
	}

	@Test
	public void validacaoFalha() {
		builder.addValidatorFromExplode(x -> true, (Function<String, Explode>) Explode::new).generateCycle().validateAndPersist("teste");

		assertFalse("não iniciou transação", iniciouTransacao.getAsBoolean());
		assertFalse("não commitou transação", deuCommit.getAsBoolean());
		assertFalse("não deu rollback na transação", deuRollback.getAsBoolean());

		assertFalse("não deveria persistir o primeiro", persistiu1.getAsBoolean());
		assertFalse("não deveria persistir o segundo", persistiu2.getAsBoolean());
		assertFalse("esse persister jamais deveria ser chamado", naoDeveriaPersistir.getAsBoolean());
	}

	@Test
	public void validacaoDuvida() {
		builder.addValidatorFromDoubt(x -> true, Doubt::new).generateCycle().validateAndPersist("teste");

		assertFalse("não iniciou transação", iniciouTransacao.getAsBoolean());
		assertFalse("não commitou transação", deuCommit.getAsBoolean());
		assertFalse("não deu rollback na transação", deuRollback.getAsBoolean());

		assertFalse("não deveria persistir o primeiro", persistiu1.getAsBoolean());
		assertFalse("não deveria persistir o segundo", persistiu2.getAsBoolean());
		assertFalse("esse persister jamais deveria ser chamado", naoDeveriaPersistir.getAsBoolean());
	}

	@Test
	public void validacaoDuvidaConfirmadaSalvaCommit() {
		ValidationCycle<String> cycle = builder.addValidatorFromDoubt(x -> true, Doubt::new).generateCycle();
		cycle.validateAndPersist("teste");

		assertFalse("não iniciou transação", iniciouTransacao.getAsBoolean());
		assertFalse("não commitou transação", deuCommit.getAsBoolean());
		assertFalse("não deu rollback na transação", deuRollback.getAsBoolean());

		assertFalse("não deveria persistir o primeiro", persistiu1.getAsBoolean());
		assertFalse("não deveria persistir o segundo", persistiu2.getAsBoolean());
		assertFalse("esse persister jamais deveria ser chamado", naoDeveriaPersistir.getAsBoolean());

		cycle.modifyAndPersist(new PrometheusDTO<>("teste"));

		assertTrue("iniciou transação", iniciouTransacao.getAsBoolean());
		assertTrue("commitou transação", deuCommit.getAsBoolean());
		assertFalse("sem rollback na transação", deuRollback.getAsBoolean());

		assertTrue("deveria persistir o primeiro", persistiu1.getAsBoolean());
		assertTrue("deveria persistir o segundo", persistiu2.getAsBoolean());
		assertFalse("esse persister jamais deveria ser chamado", naoDeveriaPersistir.getAsBoolean());
	}

	@Test
	public void validacaoDuvidaConfirmadaFalhaRollback() {
		ValidationCycle<String> cycle = builder.addPersister(x -> true, t -> {
			throw new SkipTestException();
		}).addValidatorFromDoubt(x -> true, Doubt::new).generateCycle();
		cycle.validateAndPersist("teste");

		assertFalse("não iniciou transação", iniciouTransacao.getAsBoolean());
		assertFalse("não commitou transação", deuCommit.getAsBoolean());
		assertFalse("não deu rollback na transação", deuRollback.getAsBoolean());

		assertFalse("não deveria persistir o primeiro", persistiu1.getAsBoolean());
		assertFalse("não deveria persistir o segundo", persistiu2.getAsBoolean());
		assertFalse("esse persister jamais deveria ser chamado", naoDeveriaPersistir.getAsBoolean());

		try {
			cycle.modifyAndPersist(new PrometheusDTO<>("teste"));
		} catch (SkipTestException e) {
		}

		assertTrue("iniciou transação", iniciouTransacao.getAsBoolean());
		assertFalse("não commitou transação", deuCommit.getAsBoolean());
		assertTrue("rollback na transação", deuRollback.getAsBoolean());

		assertFalse("não deveria persistir o primeiro", persistiu1.getAsBoolean());
		assertFalse("não deveria persistir o segundo", persistiu2.getAsBoolean());
		assertFalse("esse persister jamais deveria ser chamado", naoDeveriaPersistir.getAsBoolean());
	}

	private static class SkipTestException extends RuntimeException {
		private static final long serialVersionUID = 3090905716510004317L;
	}
}
