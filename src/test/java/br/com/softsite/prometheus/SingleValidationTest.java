package br.com.softsite.prometheus;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import br.com.softsite.prometheus.entity.Explode;
import br.com.softsite.prometheus.entity.PrometheusDTO;
import br.com.softsite.prometheus.lifecycle.CycleBuilder;
import br.com.softsite.prometheus.lifecycle.ValidationCycle;

public class SingleValidationTest {

	@Test
	public void singleCycle() {
		ValidationCycle<Integer> parNaoMultiplo4 = new CycleBuilder<Integer>()
				.addValidatorFromExplode(i -> i % 2 == 0, i -> i % 4 == 0? new Explode("Múltiplo de 4"): null)
				.generateCycle();
		PrometheusDTO<Integer> pdto1 = parNaoMultiplo4.validate(1);
		PrometheusDTO<Integer> pdto2 = parNaoMultiplo4.validate(2);
		PrometheusDTO<Integer> pdto4 = parNaoMultiplo4.validate(4);

		assertNotNull(pdto1);
		assertNotNull(pdto2);
		assertNotNull(pdto4);
	}
}
