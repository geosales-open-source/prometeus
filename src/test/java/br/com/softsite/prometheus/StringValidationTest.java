package br.com.softsite.prometheus;

import static org.junit.Assert.*;

import java.util.function.BooleanSupplier;

import org.junit.Before;
import org.junit.Test;

import br.com.softsite.prometheus.entity.Doubt;
import br.com.softsite.prometheus.entity.Explode;
import br.com.softsite.prometheus.entity.PrometheusDTO;
import br.com.softsite.prometheus.lifecycle.CycleBuilder;
import br.com.softsite.prometheus.lifecycle.Frankenstein;
import br.com.softsite.prometheus.lifecycle.ValidationCycle;

public class StringValidationTest {

	ValidationCycle<StringBuilder> validation2StringBuilder;
	BooleanSupplier salvou;
	BooleanSupplier validatorBomAtivado;
	BooleanSupplier validatorRuimAtivado;

	@Before
	public void createValidationCycle2StringBuilder() {
		salvou = validatorBomAtivado = validatorRuimAtivado = () -> false;
		validation2StringBuilder = new CycleBuilder<StringBuilder>().addValidator(CycleBuilder.always(), strb -> {
			validatorBomAtivado = () -> true;
			if ("TESTE".equalsIgnoreCase(strb.toString())) {
				if ("TESTE".equals(strb.toString())) {
					return Frankenstein.fromExplode(new Explode("cabum"));
				} else {
					return Frankenstein.fromDoubt(new Doubt("pergunta"));
				}
			}
			return Frankenstein.getEmpty();
		}).addModifier(CycleBuilder.always(), strb -> {
			String modif = strb.toString().toUpperCase();
			strb.setLength(0);
			strb.append(modif);
		}).addPersister(CycleBuilder.always(), strb -> {
			System.err.println("objeto sendo salvo: " + strb.toString());
			salvou = () -> true;
		}).addValidatorFromExplode(x -> false, strb -> {
			validatorRuimAtivado = () -> true;
			System.err.println("não deveria passar aqui");
			return new Explode("não deveria passar aqui");
		}).generateCycle();
	}

	@Test
	public void emptyTest() {
		assertFalse("Teste vazio não deveria ativar o 'salvou'", salvou.getAsBoolean());
		assertFalse("Teste vazio não deveria ativar o 'validatorBomAtivado'", validatorBomAtivado.getAsBoolean());
		assertFalse("Nenhum teste deveria ativar o 'validatorRuimAtivado'", validatorRuimAtivado.getAsBoolean());
	}

	@Test
	public void str__teste__Test() {
		String strParam = "teste";
		PrometheusDTO<StringBuilder> testeDto = validation2StringBuilder.validateAndPersist(new StringBuilder(strParam));

		assertFalse("Teste da string 'teste' não deveria ativar o 'salvou'", salvou.getAsBoolean());
		assertTrue("Teste da string 'teste' deveria ativar o 'validatorBomAtivado'", validatorBomAtivado.getAsBoolean());
		assertFalse("Nenhum teste deveria ativar o 'validatorRuimAtivado'", validatorRuimAtivado.getAsBoolean());

		assertEquals(testeDto.getObj().toString(), strParam);
		assertFalse(testeDto.allOk());
		assertFalse(testeDto.hasExplodes());
		assertTrue(testeDto.hasDoubts());
	}

	@Test
	public void str__TESTE__Test() {
		String strParam = "TESTE";
		PrometheusDTO<StringBuilder> testeDto = validation2StringBuilder.validateAndPersist(new StringBuilder(strParam));

		assertFalse("Teste da string 'TESTE' não deveria ativar o 'salvou'", salvou.getAsBoolean());
		assertTrue("Teste da string 'TESTE' deveria ativar o 'validatorBomAtivado'", validatorBomAtivado.getAsBoolean());
		assertFalse("Nenhum teste deveria ativar o 'validatorRuimAtivado'", validatorRuimAtivado.getAsBoolean());

		assertEquals(testeDto.getObj().toString(), strParam);
		assertFalse(testeDto.allOk());
		assertTrue(testeDto.hasExplodes());
		assertFalse(testeDto.hasDoubts());
	}

	@Test
	public void str__OUTRO__Test() {
		String strParam = "OUTRO";
		PrometheusDTO<StringBuilder> testeDto = validation2StringBuilder.validateAndPersist(new StringBuilder(strParam));

		assertTrue("Teste da string 'OUTRO' não deveria ativar o 'salvou'", salvou.getAsBoolean());
		assertTrue("Teste da string 'OUTRO' deveria ativar o 'validatorBomAtivado'", validatorBomAtivado.getAsBoolean());
		assertFalse("Nenhum teste deveria ativar o 'validatorRuimAtivado'", validatorRuimAtivado.getAsBoolean());

		assertEquals(testeDto.getObj().toString(), "OUTRO");
		assertTrue(testeDto.allOk());
	}

	@Test
	public void str__outro__Test() {
		String strParam = "outro";
		PrometheusDTO<StringBuilder> testeDto = validation2StringBuilder.validateAndPersist(new StringBuilder(strParam));

		assertTrue("Teste da string 'outro' não deveria ativar o 'salvou'", salvou.getAsBoolean());
		assertTrue("Teste da string 'outro' deveria ativar o 'validatorBomAtivado'", validatorBomAtivado.getAsBoolean());
		assertFalse("Nenhum teste deveria ativar o 'validatorRuimAtivado'", validatorRuimAtivado.getAsBoolean());

		assertEquals(testeDto.getObj().toString(), "OUTRO");
		assertTrue(testeDto.allOk());
	}
}
