package br.com.softsite.prometheus;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.function.Function;

import org.junit.Test;

import br.com.softsite.prometheus.entity.Doubt;
import br.com.softsite.prometheus.entity.Explode;
import br.com.softsite.prometheus.entity.Inform;
import br.com.softsite.prometheus.entity.PrometheusDTO;
import br.com.softsite.prometheus.lifecycle.CycleBuilder;
import br.com.softsite.prometheus.lifecycle.ValidationCycle;

public class TransformerValidationTest {

	@Test
	public void testAddValidatorFromExplodeIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.addValidatorFromExplode(Object::toString, x -> true,
						strb -> strb.length() > 3 ? new Explode("aaaaa") : null).generateCycle();

		assertTrue(compareString.validate("test").hasExplodes());
		assertFalse(compareString.validate("tes").hasExplodes());
	}

	@Test
	public void testAddValidatorFromExplodeIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.addValidatorFromExplode(Object::toString, x -> false, strb -> strb.length() > 3 ? new Explode("aaaaa") : null).generateCycle();

		assertFalse(compareString.validate("test").hasExplodes());
	}

	@Test
	public void testAddValidatorFromExplodesIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.addValidatorFromExplodes(Object::toString, x -> true,
						strb -> {
							if (strb.length() > 3) {
								ArrayList<Explode> auxList = new ArrayList<>();
								auxList.add(new Explode("aaaaa"));
								return auxList;
							}
							return new ArrayList<>();
						}).generateCycle();

		assertTrue(compareString.validate("test").hasExplodes());
		assertFalse(compareString.validate("tes").hasExplodes());
	}

	@Test
	public void testAddValidatorFromExplodesIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.addValidatorFromExplodes(Object::toString, x -> false,
						strb -> {
							if (strb.length() > 3) {
								ArrayList<Explode> auxList = new ArrayList<>();
								auxList.add(new Explode("aaa"));
								return auxList;
							}
							return null;
						}).generateCycle();

		assertFalse(compareString.validate("test").hasExplodes());
	}

	@Test
	public void testAddValidatorFromDoubtIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.addValidatorFromDoubt(Object::toString,
						x -> true,
						strb -> strb.length() > 3 ? new Doubt("oie") : null).generateCycle();
		
		assertTrue(compareString.validate("test").hasDoubts());
		assertFalse(compareString.validate("tes").hasDoubts());
	}

	@Test
	public void testAddValidatorFromDoubtIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.addValidatorFromDoubt(Object::toString,
						x -> false,
						strb -> strb.length() > 3 ? new Doubt("aaaaa") : null).generateCycle();

		assertFalse(compareString.validate("test").hasDoubts());
	}

	@Test
	public void testAddValidatorFromDoubtsTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.addValidatorFromDoubts(Object::toString, x -> true,
						strb -> {
							if (strb.length() > 3) {
								ArrayList<Doubt> auxList = new ArrayList<>();
								auxList.add(new Doubt("aaaaa"));
								return auxList;
							}
							return new ArrayList<>();
						}).generateCycle();

		assertTrue(compareString.validate("test").hasDoubts());
		assertFalse(compareString.validate("tes").hasDoubts());
	}

	@Test
	public void testAddValidatorFromDoubtsFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.addValidatorFromDoubts(Object::toString, x -> false,
						strb -> {
							if (strb.length() > 3) {
								ArrayList<Doubt> auxList = new ArrayList<>();
								auxList.add(new Doubt("aaa"));
								return auxList;
							}
							return null;
						}).generateCycle();

		assertFalse(compareString.validate("test").hasDoubts());
	}

	@Test
	public void testAddValidatorFromInformIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.addValidatorFromInform(Object::toString, x -> true,
						strb -> strb.length() > 3 ? new Inform("aaaaa") : null).generateCycle();
	
		assertTrue(compareString.validate("test").hasInforms());
		assertFalse(compareString.validate("tes").hasInforms());
	}

	@Test
	public void testAddValidatorFromInformIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.addValidatorFromInform(Object::toString, x -> false,
						strb -> strb.length() > 3 ? new Inform("aaaaa") : null).generateCycle();

		assertFalse(compareString.validate("test").hasInforms());
	}

	@Test
	public void testAddValidatorFromInformsIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.addValidatorFromInforms(Object::toString, x -> true,
						strb -> {
							if (strb.length() > 3) {
								ArrayList<Inform> auxList = new ArrayList<>();
								auxList.add(new Inform("aaaaa"));
								return auxList;
							}
							return new ArrayList<>();
						}).generateCycle();

		assertTrue(compareString.validate("test").hasInforms());
		assertFalse(compareString.validate("tes").hasInforms());
	}

	@Test
	public void testAddValidatorFromInformsIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.addValidatorFromInforms(Object::toString, x -> false,
						strb -> {
							if (strb.length() > 3) {
								ArrayList<Inform> auxList = new ArrayList<>();
								auxList.add(new Inform("aaa"));
								return auxList;
							}
							return null;
						}).generateCycle();

		assertFalse(compareString.validate("test").hasInforms());
	}

	@Test
	public void testAddModifierFromInformIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.addModifierFromInform(Object::toString, x -> true,
						strb -> strb.length() > 3 ? new Inform("aaaaa") : null).generateCycle();
		
		assertTrue(compareString.modifyAndPersist(new PrometheusDTO<>("teste")).hasInforms());
		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("tes")).hasInforms());
	}

	@Test
	public void testAddModifierFromInformIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.addModifierFromInform(Object::toString, x -> false, strb ->
						strb.length() > 3 ? new Inform("aaaaa") : null).generateCycle();

		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
	}

	@Test
	public void testAddModifierFromInformsIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.addModifierFromInforms(Object::toString, x -> true,
						strb -> {
							if (strb.length() > 3) {
								ArrayList<Inform> auxList = new ArrayList<>();
								auxList.add(new Inform("aaaaa"));
								return auxList;
							}
							return new ArrayList<>();
						}).generateCycle();

		assertTrue(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("tes")).hasInforms());
	}

	@Test
	public void testAddModifierFromInformsIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.addModifierFromInforms(Object::toString, x -> false,
						strb -> {
							if (strb.length() > 3) {
								ArrayList<Inform> auxList = new ArrayList<>();
								auxList.add(new Inform("aaa"));
								return auxList;
							}
							return null;
						}).generateCycle();

		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
	}

	@Test
	public void testAddPersisterFromInformIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.addPersisterFromInform(Object::toString, x -> true,
						strb -> strb.length() > 3 ? new Inform("aaaaa") : null).generateCycle();

		assertTrue(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("tes")).hasInforms());
	}

	@Test
	public void testAddPersisterFromInformIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.addPersisterFromInform(Object::toString, x -> false, strb ->
						strb.length() > 3 ? new Inform("aaaaa") : null).generateCycle();

		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
	}

	@Test
	public void testAddValidatorFromExplodeBiFunctionIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromExplode(Object::toString, x -> true,
						(strb, cycleCache) -> cycleCache.getCache("a", strb)
								.equals("TEST") ? new Explode("aaaaa") : null).generateCycle();

		assertTrue(compareString.validate("test").hasExplodes());
		assertFalse(compareString.validate("tes").hasExplodes());
	}

	@Test
	public void testAddValidatorFromExplodeBiFunctionIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromExplode(Object::toString, x -> true,
						(strb, cycleCache) -> cycleCache.getCache("a", strb)
								.equals("TEST") ? new Explode("aaaaa") : null).generateCycle();

		assertFalse(compareString.validate("tes").hasExplodes());
	}

	@Test
	public void testAddValidatorFromExplodesBiFunctionIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromExplodes(Object::toString, x -> true,
						(strb, cycleCache) -> {
							if (cycleCache.getCache("a", strb).equals("TEST")) {
								ArrayList<Explode> auxList = new ArrayList<>();
								auxList.add(new Explode("aaaaa"));
								return auxList;
							}
							return new ArrayList<>();
						}).generateCycle();

		assertTrue(compareString.validate("test").hasExplodes());
		assertFalse(compareString.validate("tes").hasExplodes());
	}

	@Test
	public void testAddValidatorFromExplodesBiFunctionIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromExplodes(Object::toString, x -> true,
						(strb, cycleCache) -> {
							if (cycleCache.getCache("a", strb).equals("TEST")) {
								ArrayList<Explode> auxList = new ArrayList<>();
								auxList.add(new Explode("aaaaa"));
								return auxList;
							}
							return new ArrayList<>();
						}).generateCycle();

		assertFalse(compareString.validate("tes").hasExplodes());
	}

	@Test
	public void testAddValidatorFromDoubtBiFunctionIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromDoubt(Object::toString, x -> true,
						(strb, cycleCache) -> cycleCache.getCache("a", strb)
								.equals("TEST") ? new Doubt("aaaaa") : null).generateCycle();

		assertTrue(compareString.validate("test").hasDoubts());
		assertFalse(compareString.validate("tes").hasDoubts());
	}

	@Test
	public void testAddValidatorFromDoubtBiFunctionIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromDoubt(Object::toString, x -> false,
						(strb, cycleCache) -> cycleCache.getCache("a", strb)
								.equals("TEST") ? new Doubt("aaaaa") : null).generateCycle();

		assertFalse(compareString.validate("test").hasDoubts());
	}

	@Test
	public void testAddValidatorFromDoubtsBiFunctionIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromDoubts(Object::toString, x -> true,
						(strb, cycleCache) -> {
							if (cycleCache.getCache("a", strb).equals("TEST")) {
								ArrayList<Doubt> auxList = new ArrayList<>();
								auxList.add(new Doubt("aaaaa"));
								return auxList;
							}
							return new ArrayList<>();
						}).generateCycle();

		assertTrue(compareString.validate("test").hasDoubts());
		assertFalse(compareString.validate("tes").hasDoubts());
	}

	@Test
	public void testAddValidatorFromDoubtsBiFunctionIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromDoubts(Object::toString, x -> false,
						(strb, cycleCache) -> {
							if (cycleCache.getCache("a", strb).equals("TEST")) {
								ArrayList<Doubt> auxList = new ArrayList<>();
								auxList.add(new Doubt("aaaaa"));
								return auxList;
							}
							return new ArrayList<>();
						}).generateCycle();

		assertFalse(compareString.validate("test").hasExplodes());
	}

	@Test
	public void testAddValidatorFromInformBiFunctionIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromInform(Object::toString, x -> true,
						(strb, cycleCache) -> cycleCache.getCache("a", strb)
								.equals("TEST") ? new Inform("aaaaa") : null).generateCycle();

		assertTrue(compareString.validate("test").hasInforms());
		assertFalse(compareString.validate("tes").hasInforms());
	}

	@Test
	public void testAddValidatorFromInformBiFunctionIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromInform(Object::toString, x -> false,
						(strb, cycleCache) -> cycleCache.getCache("a", strb)
								.equals("TEST") ? new Inform("aaaaa") : null).generateCycle();

		assertFalse(compareString.validate("test").hasInforms());
	}

	@Test
	public void testAddValidatorFromInformsBiFunctionIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromInforms(Object::toString, x -> true,
						(strb, cycleCache) -> {
							if (cycleCache.getCache("a", strb).equals("TEST")) {
								ArrayList<Inform> auxList = new ArrayList<>();
								auxList.add(new Inform("aaaaa"));
								return auxList;
							}
							return new ArrayList<>();
						}).generateCycle();

		assertTrue(compareString.validate("test").hasInforms());
		assertFalse(compareString.validate("tes").hasInforms());

	}

	@Test
	public void testAddValidatorFromInformsBiFunctionIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromInforms(Object::toString, x -> false,
						(strb, cycleCache) -> {
							if (cycleCache.getCache("a", strb).equals("TEST")) {
								ArrayList<Inform> auxList = new ArrayList<>();
								auxList.add(new Inform("aaaaa"));
								return auxList;
							}
							return new ArrayList<>();
						}).generateCycle();

		assertFalse(compareString.validate("test").hasInforms());
	}

	@Test
	public void testAddModifierFromInformBiFunctionIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addModifierFromInform(Object::toString, x -> true,
						(strb, cycleCache) -> cycleCache.getCache("a", strb)
								.equals("TEST") ? new Inform("aaaaa") : null).generateCycle();

		assertTrue(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("tes")).hasInforms());
	}

	@Test
	public void testAddModifierFromInformBiFunctionIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addModifierFromInform(Object::toString, x -> false,
						(strb, cycleCache) -> cycleCache.getCache("a", strb)
								.equals("TEST") ? new Inform("aaaaa") : null).generateCycle();

		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
	}

	@Test
	public void testAddModifierFromInformsBiFunctionIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addModifierFromInforms(Object::toString, x -> true,
						(strb, cycleCache) -> {
							if (cycleCache.getCache("a", strb).equals("TEST")) {
								ArrayList<Inform> auxList = new ArrayList<>();
								auxList.add(new Inform("aaaaa"));
								return auxList;
							}
							return new ArrayList<>();
						}).generateCycle();

		assertTrue(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("tes")).hasInforms());
	}

	@Test
	public void testAddModifierFromInformsBiFunctionIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addModifierFromInforms(Object::toString, x -> false,
						(strb, cycleCache) -> {
							if (cycleCache.getCache("a", strb).equals("TEST")) {
								ArrayList<Inform> auxList = new ArrayList<>();
								auxList.add(new Inform("aaaaa"));
								return auxList;
							}
							return new ArrayList<>();
						}).generateCycle();

		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
	}

	@Test
	public void testAddPersisterFromInformBiFunctionIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addPersisterFromInform(Object::toString, x -> true,
						(strb, cycleCache) -> cycleCache.getCache("a", strb)
								.equals("TEST") ? new Inform("aaaaa") : null).generateCycle();

		assertTrue(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("tes")).hasInforms());
	}

	@Test
	public void testAddPersisterFromInformBiFunctionIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addPersisterFromInform(Object::toString, x -> false,
						(strb, cycleCache) -> cycleCache.getCache("a", strb)
								.equals("TEST") ? new Inform("aaaaa") : null).generateCycle();

		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
	}

	@Test
	public void testAddPersisterFromInformsBiFunctionIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addPersisterFromInforms(Object::toString, x -> true,
						(strb, cycleCache) -> {
							if (cycleCache.getCache("a", strb).equals("TEST")) {
								ArrayList<Inform> auxList = new ArrayList<>();
								auxList.add(new Inform("aaaaa"));
								return auxList;
							}
							return new ArrayList<>();
						}).generateCycle();

		assertTrue(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("tes")).hasInforms());
	}

	@Test
	public void testAddPersisterFromInformsBiFunctionIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addPersisterFromInforms(Object::toString, x -> false,
						(strb, cycleCache) -> {
							if (cycleCache.getCache("a", strb).equals("TEST")) {
								ArrayList<Inform> auxList = new ArrayList<>();
								auxList.add(new Inform("aaaaa"));
								return auxList;
							}
							return new ArrayList<>();
						}).generateCycle();

		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
	}

	@Test
	public void testAddValidatorFromExplodeBiPredicateBiFunctionIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromExplode(Object::toString, (x,cc) -> true,
						(strb, cycleCache) -> cycleCache.getCache("a", strb)
								.equals("TEST") ? new Explode("aaaaa") : null).generateCycle();

		assertTrue(compareString.validate("test").hasExplodes());
		assertFalse(compareString.validate("tes").hasExplodes());
	}

	@Test
	public void testAddValidatorFromExplodeBiPredicateBiFunctionIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromExplode(Object::toString, (x,cc) -> false,
						(strb, cycleCache) -> cycleCache.getCache("a", strb)
								.equals("TEST") ? new Explode("aaaaa") : null).generateCycle();

		assertFalse(compareString.validate("test").hasExplodes());
	}

	@Test
	public void testAddValidatorFromExplodesBiPredicateBiFunctionIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromExplodes(Object::toString, (x,cc) -> true,
						(strb, cycleCache) -> {
							if (cycleCache.getCache("a", strb).equals("TEST")) {
								ArrayList<Explode> auxList = new ArrayList<>();
								auxList.add(new Explode("aaaaa"));
								return auxList;
							}
							return new ArrayList<>();
						}).generateCycle();

		assertTrue(compareString.validate("test").hasExplodes());
		assertFalse(compareString.validate("tes").hasExplodes());
	}

	@Test
	public void testAddValidatorFromExplodesBiPredicateBiFunctionIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromExplodes(Object::toString, (x,cc) -> false,
						(strb, cycleCache) -> {
							if (cycleCache.getCache("a", strb).equals("TEST")) {
								ArrayList<Explode> auxList = new ArrayList<>();
								auxList.add(new Explode("aaaaa"));
								return auxList;
							}
							return new ArrayList<>();
						}).generateCycle();

		assertFalse(compareString.validate("test").hasExplodes());
	}

	@Test
	public void testAddValidatorFromDoubtBiPredicateBiFunctionIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromDoubt(Object::toString, (x,cc) -> true,
						(strb, cycleCache) -> cycleCache.getCache("a", strb)
								.equals("TEST") ? new Doubt("aaaaa") : null).generateCycle();

		assertTrue(compareString.validate("test").hasDoubts());
		assertFalse(compareString.validate("tes").hasDoubts());
	}

	@Test
	public void testAddValidatorFromDoubtBiPredicateBiFunctionIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromDoubt(Object::toString, (x,cc) -> false,
						(strb, cycleCache) -> cycleCache.getCache("a", strb)
								.equals("TEST") ? new Doubt("aaaaa") : null).generateCycle();

		assertFalse(compareString.validate("test").hasDoubts());
	}

	@Test
	public void testAddValidatorFromDoubtsBiPredicateBiFunctionIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromDoubts(Object::toString, (x,cc) -> true,
						(strb, cycleCache) -> {
							if (cycleCache.getCache("a", strb).equals("TEST")) {
								ArrayList<Doubt> auxList = new ArrayList<>();
								auxList.add(new Doubt("aaaaa"));
								return auxList;
							}
							return new ArrayList<>();
						}).generateCycle();

		assertTrue(compareString.validate("test").hasDoubts());
		assertFalse(compareString.validate("tes").hasDoubts());
	}

	@Test
	public void testAddValidatorFromDoubtsBiPredicateBiFunctionIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromDoubts(Object::toString, (x,cc) -> false,
						(strb, cycleCache) -> {
							if (cycleCache.getCache("a", strb).equals("TEST")) {
								ArrayList<Doubt> auxList = new ArrayList<>();
								auxList.add(new Doubt("aaaaa"));
								return auxList;
							}
							return new ArrayList<>();
						}).generateCycle();

		assertFalse(compareString.validate("test").hasDoubts());
	}

	@Test
	public void testAddValidatorFromInformBiPredicateBiFunctionIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromInform(Object::toString, (x,cc) -> true,
						(strb, cycleCache) -> cycleCache.getCache("a", strb)
								.equals("TEST") ? new Inform("aaaaa") : null).generateCycle();

		assertTrue(compareString.validate("test").hasInforms());
		assertFalse(compareString.validate("tes").hasInforms());
	}

	@Test
	public void testAddValidatorFromInformBiPredicateBiFunctionIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromInform(Object::toString, (x,cc) -> false,
						(strb, cycleCache) -> cycleCache.getCache("a", strb)
								.equals("TEST") ? new Inform("aaaaa") : null).generateCycle();

		assertFalse(compareString.validate("test").hasInforms());
	}

	@Test
	public void testAddValidatorFromInformsBiPredicateBiFunctionIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromInforms(Object::toString, (x,cc) -> true,
						(strb, cycleCache) -> {
							if (cycleCache.getCache("a", strb).equals("TEST")) {
								ArrayList<Inform> auxList = new ArrayList<>();
								auxList.add(new Inform("aaaaa"));
								return auxList;
							}
							return new ArrayList<>();
						}).generateCycle();

		assertTrue(compareString.validate("test").hasInforms());
		assertFalse(compareString.validate("tes").hasInforms());
	}

	@Test
	public void testAddValidatorFromInformsBiPredicateBiFunctionIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
				.registerCache("a", (Function<String, String>) String::toUpperCase)
				.addValidatorFromInforms(Object::toString, (x,cc) -> false,
						(strb, cycleCache) -> {
							if (cycleCache.getCache("a", strb).equals("TEST")) {
								ArrayList<Inform> auxList = new ArrayList<>();
								auxList.add(new Inform("aaaaa"));
								return auxList;
							}
							return new ArrayList<>();
						}).generateCycle();

		assertFalse(compareString.validate("test").hasInforms());
	}

	@Test
	public void testAddPersisterFromInformsIsAliveTruee() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addModifierFromInforms(Object::toString, x -> true,
				strb -> {
					if (strb.length() > 3) {
						ArrayList<Inform> auxList = new ArrayList<>();
						auxList.add(new Inform("aaaaa"));
						return auxList;
					}
					return new ArrayList<>();
				}).generateCycle();

		assertTrue(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("tes")).hasInforms());
	}

	@Test
	public void testAddPersisterFromInformsIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addModifierFromInforms(Object::toString, x -> false,
				strb -> {
					if (strb.length() > 3) {
						ArrayList<Inform> auxList = new ArrayList<>();
						auxList.add(new Inform("aaa"));
						return auxList;
					}
					return null;
				}).generateCycle();

		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
	}

	@Test
	public void testAddModifierFromInformBiPredicateBiFunctionIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addModifierFromInform(Object::toString, (x, cc) -> true,
				strb -> strb.length() > 3 ? new Inform("aaaaa") : null).generateCycle();

		assertTrue(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("tes")).hasInforms());
	}

	@Test
	public void testAddModifierFromInformBiPredicateBiFunctionIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addModifierFromInform(Object::toString, (x, cc) -> false,
				strb -> strb.length() > 3 ? new Inform("aaaaa") : null).generateCycle();

		assertFalse(compareString.validate("test").hasInforms());
	}

	@Test
	public void testAddModifierFromInformsBiPredicateBiFunctionIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addModifierFromInforms(Object::toString, (x, cc) -> true,
				strb -> {
					if (strb.length() > 3) {
						ArrayList<Inform> auxList = new ArrayList<>();
						auxList.add(new Inform("aaaaa"));
						return auxList;
					}
					return new ArrayList<>();
				}).generateCycle();

		assertTrue(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("tes")).hasInforms());
	}

	@Test
	public void testAddModifierFromInformsBiPredicateBiFunctionIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addModifierFromInforms(Object::toString, (x, cc) -> false,
				strb -> {
					if (strb.length() > 3) {
						ArrayList<Inform> auxList = new ArrayList<>();
						auxList.add(new Inform("aaaaa"));
						return auxList;
					}
					return new ArrayList<>();
				}).generateCycle();

		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
	}
	
	@Test
	public void testAddPersisterFromInformBiPredicateBiFunctionIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.registerCache("a", (Function<String, String>) String::toUpperCase)
			.addPersisterFromInform(Object::toString, (x, cc) -> true,
				(strb, cycleCache) -> cycleCache.getCache("a", strb)
					.equals("TEST") ? new Inform("aaaaa") : null).generateCycle();

		assertTrue(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("tes")).hasInforms());
	}

	@Test
	public void testAddPersisterFromInformBiPredicateBiFunctionIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addPersisterFromInform(Object::toString, (x, cc) -> false,
				strb -> strb.length() > 3 ? new Inform("aaaaa") : null).generateCycle();

		assertFalse(compareString.validate("test").hasInforms());
	}

	@Test
	public void testAddPersisterFromInformsBiPredicateBiFunctionIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addPersisterFromInforms(Object::toString, (x, cc) -> true,
				strb -> {
					if (strb.length() > 3) {
						ArrayList<Inform> auxList = new ArrayList<>();
						auxList.add(new Inform("aaaaa"));
						return auxList;
					}
					return new ArrayList<>();
				}).generateCycle();

		assertTrue(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("tes")).hasInforms());
	}

	@Test
	public void testAddPersisterFromInformsBiPredicateBiFunctionIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addPersisterFromInforms(Object::toString, (x, cc) -> false,
				strb -> {
					if (strb.length() > 3) {
						ArrayList<Inform> auxList = new ArrayList<>();
						auxList.add(new Inform("aaaaa"));
						return auxList;
					}
					return new ArrayList<>();
				}).generateCycle();

		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
	}

	@Test
	public void testAddValidatorFromExplodeBiPredicateIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addValidatorFromInform(Object::toString, (x, cc) -> true,
				strb -> strb.length() > 3 ? new Inform("aaaaa") : null).generateCycle();

		assertTrue(compareString.validate("test").hasInforms());
		assertFalse(compareString.validate("tes").hasInforms());
	}

	@Test
	public void testAddValidatorFromExplodeBiPredicateIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addValidatorFromExplode(Object::toString, (x, cc) -> false, 
				strb -> strb.length() > 3 ? new Explode("aaaaa") : null).generateCycle();

		assertFalse(compareString.validate("test").hasInforms());
	}

	@Test
	public void testAddValidatorFromExplodesBiPredicateIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addValidatorFromExplodes(Object::toString, (x, cc) -> true,
				strb -> {
					if (strb.length() > 3) {
						ArrayList<Explode> auxList = new ArrayList<>();
						auxList.add(new Explode("aaaaa"));
						return auxList;
					}
					return new ArrayList<>();
				}).generateCycle();

		assertTrue(compareString.validate("test").hasExplodes());
		assertFalse(compareString.validate("tes").hasExplodes());
	}

	@Test
	public void testAddValidatorFromExplodesBiPredicateIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addValidatorFromExplodes(Object::toString, (x, cc) -> false,
				strb -> {
					if (strb.length() > 3) {
						ArrayList<Explode> auxList = new ArrayList<>();
						auxList.add(new Explode("aaaaa"));
						return auxList;
					}
					return new ArrayList<>();
				}).generateCycle();

		assertFalse(compareString.validate("test").hasExplodes());
	}

	@Test
	public void testAddValidatorFromDoubtBiPredicateIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addValidatorFromDoubt(Object::toString, (x, cc) -> true,
				strb -> strb.length() > 3 ? new Doubt("aaaaa") : null).generateCycle();

		assertTrue(compareString.validate("test").hasDoubts());
		assertFalse(compareString.validate("tes").hasDoubts());
	}

	@Test
	public void testAddValidatorFromDoubtBiPredicateIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addValidatorFromDoubt(Object::toString, (x, cc) -> false, strb ->
				strb.length() > 3 ? new Doubt("aaaaa") : null).generateCycle();

		assertFalse(compareString.validate("test").hasInforms());
	}

	@Test
	public void testAddValidatorFromDoubtsBiPredicateIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addValidatorFromDoubts(Object::toString,
				(x, cc) -> true,
				strb -> {
					if (strb.length() > 3) {
						ArrayList<Doubt> auxList = new ArrayList<>();
						auxList.add(new Doubt("aaaaa"));
						return auxList;
					}
					return new ArrayList<>();
				}).generateCycle();

		assertTrue(compareString.validate("test").hasDoubts());
		assertFalse(compareString.validate("tes").hasDoubts());
	}

	@Test
	public void testAddValidatorFromDoubtsBiPredicateIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addValidatorFromDoubts(Object::toString, (x, cc) -> false,
				strb -> {
					if (strb.length() > 3) {
						ArrayList<Doubt> auxList = new ArrayList<>();
						auxList.add(new Doubt("aaaaa"));
						return auxList;
					}
					return new ArrayList<>();
				}).generateCycle();

		assertFalse(compareString.validate("test").hasDoubts());
	}

	@Test
	public void testAddValidatorFromInformBiPredicateIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addValidatorFromInform(Object::toString, (x, cc) -> true,
				strb -> strb.length() > 3 ? new Inform("aaaaa") : null).generateCycle();

		assertTrue(compareString.validate("test").hasInforms());
		assertFalse(compareString.validate("tes").hasInforms());
	}

	@Test
	public void testAddValidatorFromInformBiPredicateIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addValidatorFromInform(Object::toString, (x, cc) -> false, strb ->
				strb.length() > 3 ? new Inform("aaaaa") : null).generateCycle();

		assertFalse(compareString.validate("test").hasInforms());
	}

	@Test
	public void testAddValidatorFromInformsBiPredicateIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addValidatorFromInforms(Object::toString, (x, cc) -> true,
				strb -> {
					if (strb.length() > 3) {
						ArrayList<Inform> auxList = new ArrayList<>();
						auxList.add(new Inform("aaaaa"));
						return auxList;
					}
					return new ArrayList<>();
				}).generateCycle();

		assertTrue(compareString.validate("test").hasInforms());
		assertFalse(compareString.validate("tes").hasInforms());
	}

	@Test
	public void testAddValidatorFromInformsBiPredicateIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addValidatorFromInforms(Object::toString, (x, cc) -> false,
				strb -> {
					if (strb.length() > 3) {
						ArrayList<Inform> auxList = new ArrayList<>();
						auxList.add(new Inform("aaaaa"));
						return auxList;
					}
					return new ArrayList<>();
				}).generateCycle();

		assertFalse(compareString.validate("test").hasInforms());
	}

	@Test
	public void testAddModifierFromInformBiPredicateIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addPersisterFromInform(Object::toString, (x, cc) -> true,
				strb -> strb.length() > 3 ? new Inform("aaaaa") : null).generateCycle();

		assertTrue(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("tes")).hasInforms());
	}

	@Test
	public void testAddModifierFromInformBiPredicateIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addPersisterFromInform(Object::toString, (x, cc) -> false,
				strb -> strb.length() > 3 ? new Inform("aaaaa") : null).generateCycle();

		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
	}

	@Test
	public void testAddModifierFromInformsBiPredicateIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addModifierFromInforms(Object::toString, (x, cc) -> true,
				strb -> {
					if (strb.length() > 3) {
						ArrayList<Inform> auxList = new ArrayList<>();
						auxList.add(new Inform("aaaaa"));
						return auxList;
					}
					return new ArrayList<>();
				}).generateCycle();

		assertTrue(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("tes")).hasInforms());
	}

	@Test
	public void testAddModifierFromInformsBiPredicateIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addPersisterFromInforms(Object::toString, (x, cc) -> false,
				strb -> {
					if (strb.length() > 3) {
						ArrayList<Inform> auxList = new ArrayList<>();
						auxList.add(new Inform("aaaaa"));
						return auxList;
					}
					return new ArrayList<>();
				}).generateCycle();

		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
	}

	@Test
	public void testAddPersisterFromInformBiPredicateIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addPersisterFromInform(Object::toString, (x, cc) -> true,
				strb -> strb.length() > 3 ? new Inform("aaaaa") : null).generateCycle();

		assertTrue(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("tes")).hasInforms());
	}

	@Test
	public void testAddPersisterFromInformBiPredicateIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addPersisterFromInform(Object::toString, (x, cc) -> false,
				strb -> strb.length() > 3 ? new Inform("aaaaa") : null).generateCycle();

		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
	}

	@Test
	public void testAddPersisterFromInformsBiPredicateIsAliveTrue() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addPersisterFromInforms(Object::toString, (x, cc) -> true,
				strb -> {
					if (strb.length() > 3) {
						ArrayList<Inform> auxList = new ArrayList<>();
						auxList.add(new Inform("aaaaa"));
						return auxList;
					}
					return new ArrayList<>();
				}).generateCycle();

		assertTrue(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("tes")).hasInforms());
	}

	@Test
	public void testAddPersisterFromInformsBiPredicateIsAliveFalse() {
		ValidationCycle<String> compareString = new CycleBuilder<String>()
			.addPersisterFromInforms(Object::toString, (x, cc) -> false,
				strb -> {
					if (strb.length() > 3) {
						ArrayList<Inform> auxList = new ArrayList<>();
						auxList.add(new Inform("aaaaa"));
						return auxList;
					}
					return new ArrayList<>();
				}).generateCycle();

		assertFalse(compareString.modifyAndPersist(new PrometheusDTO<>("test")).hasInforms());
	}
}