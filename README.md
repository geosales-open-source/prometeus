Pequena biblioteca para definição de validade de dados.

Ela é definida com as seguintes características:

- _yielders_
- _validators_
- _modifiers_
- _persisters_

Cada um desses elementos precisa passar por uma validação, então, os objetos
Prometheus todos são criados com um predicado `active`.

No fluxo de inserção de dados, o usuário envia uma requisição que, após passar
pelos validadores, pode voltar com dúvidas (ou blocantes) ou então seguir
para modificar e, então, persistir. Caso não haja dúvidas, o sistema persistirá
diretamente:

```mermaid
graph LR
  user[Usuário] --> |FIRST REQ| validators
  validators --> |FAIL&ABORT| user
  validators --> |PERGUNTAS| user
  validators --> |I'M ALIVE| modifiers
  user --> |RESPOSTAS| modifiers
  modifiers --> persisters
```

Outra visão:

```mermaid
sequenceDiagram
  participant Usuário
  participant Validators
  Usuário->Validators: Isso é válido?
  Validators->Modifiers: Isso é válido, continue...
  Validators-->Usuário: blocante, abortar
  Validators-->Usuário: tenho dúvidas...
  Usuário->Modifiers: dúvidas? que tal respostas!
  Modifiers->Persisters: salva isso!
```